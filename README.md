# spark-sql

这是一个spark学习代码相关的项目 里面有很多关于spark、spark-sql、sparkstreaming等相关的实例代码 采用的语言是scala编写的，开发工具是idea。 spark是目前最为刘萧的开源大数据处理引擎，所有对大数据感兴趣的开发人验和数据科学家的标配工具。 spark里面包括有（结构化流处理、高级分析、软件库与生态系统、DataSet、结构化API【DataFrame】、SQL、RDD【低级API】、分布式变量）。